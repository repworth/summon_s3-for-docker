Docker container with: 

- [Summon](https://github.com/conjurinc/summon)
- [Summon S3 Provider](https://github.com/conjurinc/summon-s3)

## Scripts
Open shell with provided environment variables:
```
docker \
  run \
  -it \
  -e AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY \ 
  -e AWS_REGION \
  repworth/summon-s3:summon_s30.3
```